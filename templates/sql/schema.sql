CREATE TABLE people (
	id integer primary key autoincrement,
	name varchar2(255),
	birthdate DATE,
	timestamp TIMESTAMP
);

CREATE TABLE meta (
	version id integer primary key autoincrement
)
