#!/usr/bin/python

################################################################################
# Imports
################################################################################

import os
import sys

from datetime import datetime, date

from flask import Flask, render_template, request, redirect, url_for

from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.script import Manager
from flask.ext.sqlalchemy import SQLAlchemy

from flask_wtf import Form
from wtforms import StringField, SubmitField
from wtforms.fields.html5 import DateField
from wtforms.validators import Required

################################################################################
# Globals
################################################################################

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'aohsnfgiwtrnqc7otnqyo7l'

try:
	app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['DATABASE_URL']
except:
	app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://localhost/birthdate_app"

app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True

manager = Manager(app)

db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)

################################################################################
# Classes
################################################################################

class Entry(db.Model):
	__tablename__ = "entries"
	id = db.Column(db.Integer, primary_key=True)
	timestamp = db.Column(db.DateTime())
	name = db.Column(db.String(255))
	birthdate = db.Column(db.Date())

	def days_alive(self):
		td = date.today() - self.birthdate
		return td.days
	
	def __repr__(self):
		return '<Entry: %r>' % self.name

class V1_Form(Form):
	name = StringField("Name", validators=[Required()])
	birthdate = DateField("Date of Birth", format='%Y-%m-%d', validators=[Required()])
	submit = SubmitField('Submit')

################################################################################
# Functions
################################################################################

def v1_setup():
	print("Deploying Version 1")
	print("-------------------\n")
	print("Dropping existing tables...")
	db.drop_all()
	print("Creating new tables...")
	db.create_all()
	
	print("\nSetup Complete!\n")

app.config.from_object(__name__)

################################################################################
# Flask Route Functions
################################################################################

@app.route('/')
def index():
	myform = V1_Form()
	
	return render_template("index.html", form=myform, form_url=url_for('save'))

@app.route('/save', methods=['POST'])
def save():
	timestamp = datetime.now()
	name = request.form.get("name")
	birthdate = datetime.strptime(request.form.get("birthdate"), "%Y-%m-%d").date()
	
	entry = Entry(timestamp = timestamp, name = name, birthdate = birthdate)
	
	db.session.add(entry)
	db.session.commit()
	
	return redirect(url_for('result', entry_id = entry.id))

@app.route('/result/<int:entry_id>', methods=['POST', 'GET'])
def result(entry_id):
	entry = Entry.query.get_or_404(entry_id)
	return render_template("results.html", entry = entry, list_url = url_for('list'), home_url = url_for('index'))

@app.route('/list')
def list():
	results = Entry.query.all()
	
	return render_template("list.html", results=results)

################################################################################
# Main Function
################################################################################

if __name__ == "__main__":
	db.create_all()
	manager.run()
	
